window.addEventListener("DOMContentLoaded", () => {

// Дано інпути. Зробіть так, щоб усі інпути втрати фокусу перевіряли свій вміст на правильну кількість символів. Скільки символів має бути в інпуті, зазначається в атрибуті data-length. Якщо вбито правильну кількість, то межа інпуту стає зеленою, якщо неправильна – червоною.

   const [...inputs] = document.querySelectorAll("form[data-id='task1'] input");

   inputs.forEach((input) => {
      input.addEventListener("blur", (e) => {
         if (e.target.value.length == e.target.getAttribute("data-lenght")) {
            e.target.classList.add("green");
            e.target.classList.remove("red");
         } else {
            e.target.classList.remove("green");
            e.target.classList.add("red");
         }         
      })
   });

   // - При завантаженні сторінки показати користувачеві поле введення (`input`) з написом `Price`. Це поле буде служити для введення числових значень
   // - Поведінка поля має бути такою:
   // - При фокусі на полі введення – у нього має з'явитися рамка зеленого кольору. При втраті фокусу вона пропадає.
   // - Коли забрали фокус з поля - його значення зчитується, над полем створюється `span`, в якому має бути виведений текст: 
   // . 
   // Поруч із ним має бути кнопка з хрестиком (`X`). Значення всередині поля введення фарбується зеленим.
   // - При натисканні на `Х` - `span` з текстом та кнопка `X` повинні бути видалені.
   // - Якщо користувач ввів число менше 0 - при втраті фокусу підсвічувати поле введення червоною рамкою, 
   // під полем виводити фразу - `Please enter correct price`. `span` зі значенням при цьому не створюється.

   const inputPrice = document.querySelector("form[data-id='task2'] input");
   const pattern = /[0-9]/;
   const formPrice = document.querySelector("form[data-id='task2']");
   let flag = true;
   

   inputPrice.addEventListener("keypress", (e) => {
      if (!pattern.test(String.fromCharCode(e.charCode))) {
         e.preventDefault();
      }
   });

   inputPrice.addEventListener("focus", (e) => {
      e.target.classList.add("green");
   });

   inputPrice.addEventListener("blur", (e) => {
      e.target.classList.remove("red_bg"); 
      e.target.classList.remove("green");      
      if (inputPrice.value !== '' && inputPrice.value != 0 && !document.querySelector("div")) {
         if (document.querySelector('section')) {
            let section = document.querySelector('section');
            section.remove(e.target);

            e.target.classList.add("green_text");
            let div = document.createElement('div');
            div.innerHTML = `<span>${inputPrice.value}</span><input type="button" value="❌">`;

            formPrice.prepend(div);
         } else {
         e.target.classList.add("green_text");
         let div = document.createElement('div');
         div.innerHTML = `<span>${inputPrice.value}</span><input type="button" value="❌">`;

         formPrice.prepend(div);
         }
      } else if (inputPrice.value !== '' && inputPrice.value != 0 && document.querySelector("div") && inputPrice.value !== document.querySelector("span").textContent) {
         if (document.querySelector('section')) {
            let section = document.querySelector('section');
            section.remove(e.target);

            document.querySelector("span").textContent = inputPrice.value;
         } else {
            document.querySelector("span").textContent = inputPrice.value;
         }          
      } else if (inputPrice.value <= 0) {
         e.target.classList.add("red_bg");
         let section = document.createElement('section');
         section.innerHTML = `Please enter correct price`;

         formPrice.append(section);
      } else if (document.querySelector('section') && inputPrice.value > 0) {
         let section = document.querySelector('section');
         section.remove(e.target);
      };

      if (document.querySelector("div") && flag === true) {
         const btnDel = document.querySelector("input[type='button']");
   
         btnDel.addEventListener("click", (e) => {
            let div = document.querySelector("div");         
            div.remove(e.target);
            flag = true;
         });
         flag = false;   
      };
   });
})